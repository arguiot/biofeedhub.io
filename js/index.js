const P = new ProType();

class View extends P.ViewController {
  willShow() {
    this.d = this.view.querySelector(".d")
    this.h = this.view.querySelector(".h")
    this.m = this.view.querySelector(".m")
    this.s = this.view.querySelector(".s")
    
    
    this.b = moment([2018, 6])
    
    setInterval(this.render.bind(this), 100)
  }
  render() {
    const a = moment()
    const difference = this.b.diff(a)
    const diff = moment(difference).toObject()
    this.d.innerHTML = `${diff.date} d`
    this.h.innerHTML = `${diff.hours} h`
    this.m.innerHTML = `${diff.minutes} m`
    this.s.innerHTML = `${diff.seconds} s`
  }
}

P.autoMount(View)
P.set("main")
